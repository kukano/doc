# Documentation on the Kukano project

Again, I created everything just to server my purpose, I was bored on sunday afternoon, very large part of it is not
fancy code at all. Don't judge! I just had a time and a lot of Raspberry thingies lying around.

The purpose is that I wanted to detect thieves! Take a picture of them, scream at them and have fun. The project is
composed of 3 parts:

 * [kukano rpi-client](https://gitlab.com/kukano/rpi):
   * detects:
      * movement,
      * noise,
      * bluetooth MAC addresses.
   * based on the detection and missing known MAC BT addresses generates:
      * incident report to [backend](https://gitlab.com/kukano/backend),
      * firebase notification (further setup needed),
      * picture at high quality and picture with detection info.
 * [backend](https://gitlab.com/kukano/backend):
   * collects incidents and provide basic CRUD operations
 * [android-app](https://gitlab.com/kukano/android-app):
   * Flutter app to inform user about the incident





